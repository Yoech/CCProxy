package qqwry

import (
	"encoding/binary"
	"fmt"
	"golang.org/x/text/encoding/simplifiedchinese"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

// some const
const (
	IndexLen      = 7
	RedirectMode1 = 0x01
	RedirectMode2 = 0x02
)

// ResultQQwry .
type ResultQQwry struct {
	IP      string `json:"ip"`
	Country string `json:"country"`
	Area    string `json:"area"`
}

// QQwry ...
type QQwry struct {
	Path   string
	File   *os.File
	Data   []byte
	Counts int64
	Start  uint32
	End    uint32
	Offset int64
	lock   *sync.Mutex
}

// QQWryPtr .
var QQWryPtr *QQwry

// NewPool .
func NewPool(uri string) *QQwry {
	if QQWryPtr != nil {
		return QQWryPtr
	}

	_, err := os.Stat(uri)
	if err != nil && os.IsNotExist(err) {
		log.Println(err)
		return nil
	}

	QQWryPtr = &QQwry{}
	QQWryPtr.lock = new(sync.Mutex)
	QQWryPtr.Path = uri

	QQWryPtr.File, err = os.OpenFile(QQWryPtr.Path, os.O_RDONLY, 0400)
	if err != nil {
		log.Printf("OpenFile[%v].err[%v]", uri, err)
		return nil
	}
	defer QQWryPtr.File.Close()

	QQWryPtr.Data, err = ioutil.ReadAll(QQWryPtr.File)
	if err != nil {
		log.Printf("ReadAll[%v].err[%v]", uri, err)
		return nil
	}

	QQWryPtr.Start = binary.LittleEndian.Uint32(QQWryPtr.Data[:4])
	QQWryPtr.End = binary.LittleEndian.Uint32(QQWryPtr.Data[4:8])
	QQWryPtr.Counts = int64((QQWryPtr.Start-QQWryPtr.End)/IndexLen + 1)

	return QQWryPtr
}

// Version ...
// Query QQWry's version and total records.
// Cool.Cat
func (q *QQwry) Version() string {
	q.lock.Lock()
	defer q.lock.Unlock()
	rs := q.readData(3, int64(q.End+4))
	pos := binary.LittleEndian.Uint32(rs[:4]) + 4

	enc := simplifiedchinese.GBK.NewDecoder()
	country, _ := enc.String(string(q.readArea(pos)))
	location, _ := enc.String(string(q.readArea(uint32(int(pos) + len(q.readArea(pos)) + 1))))
	return fmt.Sprintf("%v%v(%v)", country, location, q.Counts)
}

// Total .
func (q *QQwry) Total() int64 {
	return q.Counts
}

// Query .
func (q *QQwry) Query(ip string) (res ResultQQwry) {
	q.lock.Lock()
	defer q.lock.Unlock()

	res = ResultQQwry{}

	res.IP = ip
	if strings.Count(ip, ".") != 3 {
		return res
	}
	offset := q.searchIndex(binary.BigEndian.Uint32(net.ParseIP(ip).To4()))
	if offset <= 0 {
		return
	}

	var country []byte
	var area []byte

	mode := q.readMode(offset + 4)
	if mode == RedirectMode1 {
		countryOffset := q.readUInt24()
		mode = q.readMode(countryOffset)
		if mode == RedirectMode2 {
			c := q.readUInt24()
			country = q.readString(c)
			countryOffset += 4
		} else {
			country = q.readString(countryOffset)
			countryOffset += uint32(len(country) + 1)
		}
		area = q.readArea(countryOffset)
	} else if mode == RedirectMode2 {
		countryOffset := q.readUInt24()
		country = q.readString(countryOffset)
		area = q.readArea(offset + 8)
	} else {
		country = q.readString(offset + 4)
		area = q.readArea(offset + uint32(5+len(country)))
	}

	enc := simplifiedchinese.GBK.NewDecoder()
	res.Country, _ = enc.String(string(country))
	res.Area, _ = enc.String(string(area))
	res.Country = strings.Replace(res.Country, "CZ88.NET", "", -1)
	res.Area = strings.Replace(res.Area, "CZ88.NET", "", -1)
	return
}

func (q *QQwry) searchIndex(ip uint32) uint32 {
	header := q.readData(8, 0)

	start := binary.LittleEndian.Uint32(header[:4])
	end := binary.LittleEndian.Uint32(header[4:])

	buf := make([]byte, IndexLen)
	var mid uint32
	var _ip uint32

	for {
		mid = q.getMiddleOffset(start, end)
		buf = q.readData(IndexLen, int64(mid))
		_ip = binary.LittleEndian.Uint32(buf[:4])

		if end-start == IndexLen {
			offset := byteToUInt32(buf[4:])
			buf = q.readData(IndexLen)
			if ip < binary.LittleEndian.Uint32(buf[:4]) {
				return offset
			}
			return 0
		}

		// 找到的比较大，向前移
		if _ip > ip {
			end = mid
		} else if _ip < ip { // 找到的比较小，向后移
			start = mid
		} else if _ip == ip {
			return byteToUInt32(buf[4:])
		}
	}
}

func (q *QQwry) setOffset(offset int64) {
	q.Offset = offset
}

func (q *QQwry) readData(num int, offset ...int64) (rs []byte) {
	if len(offset) > 0 {
		q.setOffset(offset[0])
	}
	nums := int64(num)
	end := q.Offset + nums
	dataNum := int64(len(q.Data))
	if q.Offset > dataNum {
		return nil
	}

	if end > dataNum {
		end = dataNum
	}
	rs = q.Data[q.Offset:end]
	q.Offset = end
	return
}

func (q *QQwry) readMode(offset uint32) byte {
	mode := q.readData(1, int64(offset))
	return mode[0]
}

func (q *QQwry) readArea(offset uint32) []byte {
	mode := q.readMode(offset)
	if mode == RedirectMode1 || mode == RedirectMode2 {
		areaOffset := q.readUInt24()
		if areaOffset == 0 {
			return []byte("")
		}
		return q.readString(areaOffset)
	}
	return q.readString(offset)
}

func (q *QQwry) readString(offset uint32) []byte {
	q.setOffset(int64(offset))
	data := make([]byte, 0, 30)
	for {
		buf := q.readData(1)
		if buf[0] == 0 {
			break
		}
		data = append(data, buf[0])
	}
	return data
}

func (q *QQwry) readUInt24() uint32 {
	buf := q.readData(3)
	return byteToUInt32(buf)
}

func (q *QQwry) getMiddleOffset(start uint32, end uint32) uint32 {
	records := ((end - start) / IndexLen) >> 1
	return start + records*IndexLen
}

func byteToUInt32(data []byte) uint32 {
	i := uint32(data[0]) & 0xff
	i |= (uint32(data[1]) << 8) & 0xff00
	i |= (uint32(data[2]) << 16) & 0xff0000
	return i
}
