package ccyaml

import (
	"github.com/Yoech/CCProxy/ccutility"
	"github.com/Yoech/CCProxy/config"
	"github.com/Yoech/CCProxy/dbhelper"
	"github.com/Yoech/CCProxy/myredis"
	"github.com/Yoech/CCProxy/mysql"
	"github.com/Yoech/CCProxy/qqwry"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"strings"
	"sync"
	"time"
)

// TagYamlAPIInfo .
type TagYamlAPIInfo struct {
	Listen  string `yaml:"listen"`
	Version string `yaml:"version"`
}

// TagYamlInfo .
type TagYamlInfo struct {
	QQWry  string `yaml:"qqwry"`
	Rules  string `yaml:"rules"`
	DBPath string `yaml:"dbpath"`
}

// TagYamlConfInfo .
type TagYamlConfInfo struct {
	API    TagYamlAPIInfo           `yaml:"api"`
	Info   TagYamlInfo              `yaml:"info"`
	MySQL  mysql.TagMySQLPoolInfo   `yaml:"mysql"`
	Redis  myredis.TagRedisPoolInfo `yaml:"redis"`
	Agents []string                 `yaml:"agents"`
}

// TagYamlSiteInfo .
type TagYamlSiteInfo struct {
	URI    []string `yaml:"uri"`
	Host   string   `yaml:"host"`
	Agents []string `yaml:"agents"`
	Script string   `yaml:"script"`
	Func   string   `yaml:"func"`
}

// TagYamlSelectorInfo .
type TagYamlSelectorInfo struct {
	Find  string `yaml:"find"`
	Child string `yaml:"child"`
	Idx   []int  `yaml:"idx"`
}

// TagYamlRulesInfo .
type TagYamlRulesInfo struct {
	Site     *TagYamlSiteInfo
	Selector *TagYamlSelectorInfo
}

// YamlConf .
var YamlConf *TagYamlConfInfo

// YamRules .
var YamRules []*TagYamlRulesInfo

var lockYaml = new(sync.Mutex)

// LoadConf .
func LoadConf(uri string) bool {
	yamlFile, err := ioutil.ReadFile(uri)
	if err != nil {
		log.Printf("LoadConf.ReadFile[%v].err[%v]", uri, err)
		return false
	}

	if YamlConf == nil {
		YamlConf = &TagYamlConfInfo{}
	}

	err = yaml.Unmarshal(yamlFile, YamlConf)
	if err != nil {
		log.Printf("LoadConf.Unmarshal.err[%v]", err)
		return false
	}

	if config.QQ == nil {
		config.QQ = qqwry.NewPool(YamlConf.Info.QQWry)
		if config.QQ == nil {
			return false
		}
		log.Printf("%v", config.QQ.Version())
	}

	if config.RedisPool == nil {
		config.RedisPool = myredis.NewPool(YamlConf.Redis.Addr, YamlConf.Redis.Port, YamlConf.Redis.DB, YamlConf.Redis.Pass, YamlConf.Redis.Prefix, YamlConf.Redis.MaxConn, YamlConf.Redis.IdleConn, time.Duration(YamlConf.Redis.IdleTimeout)*time.Second)
		if config.RedisPool == nil {
			return false
		}
	}
	if config.MySQL == nil {
		config.MySQL = mysql.NewPool(YamlConf.MySQL.Addr, YamlConf.MySQL.Port, YamlConf.MySQL.DB, YamlConf.MySQL.User, YamlConf.MySQL.Pass, YamlConf.MySQL.Charset, YamlConf.MySQL.MaxConn, YamlConf.MySQL.IdleConn)
		if config.MySQL == nil {
			return false
		}
	}

	if YamlConf.Info.DBPath != "" {
		// check if the sql table does not exist, rebuild.
		if ReloadSQL() == false {
			return false
		}
	}

	// always true
	return ReloadRules() >= 0
}

// ReloadRules .
func ReloadRules() int {
	lockYaml.Lock()
	defer lockYaml.Unlock()
	var allFile []string
	if YamlConf == nil {
		return 0
	}

	allFile, err := ccutility.GetAllFileByExt(YamlConf.Info.Rules, ".yaml", allFile)
	if err != nil {
		return 0
	}

	if len(allFile) < 1 {
		log.Printf("LoadRules[%v].nil", YamlConf.Info.Rules)
		return 0
	}

	var needReset = false
	var tmpRules = make([]*TagYamlRulesInfo, 0)
	for _, v := range allFile {
		yamlFile, err := ioutil.ReadFile(v)
		if err != nil {
			log.Printf("LoadRules.ReadFile[%v].err[%v]", v, err)
			return 0
		}

		yamlRules := &TagYamlRulesInfo{}

		err = yaml.Unmarshal(yamlFile, yamlRules)
		if err != nil {
			log.Printf("LoadRules.Unmarshal.err[%v]", err)
			return 0
		}

		if isNullRules(v, yamlRules) == false {
			tmpRules = append(tmpRules, yamlRules)
			needReset = true
		}
	}

	if needReset == true {
		if YamRules == nil {
			YamRules = make([]*TagYamlRulesInfo, 0)
		}
		YamRules = YamRules[0:0]
		YamRules = tmpRules
	}

	return len(YamRules)
}

func isNullRules(fileName string, r *TagYamlRulesInfo) bool {
	if r == nil {
		log.Printf("[%v] => is null!", fileName)
		return true
	}
	if r.Site == nil || r.Selector == nil {
		log.Printf("[%v] => site or selector key is null!", fileName)
		return true
	}
	for i := 0; i < len(r.Site.URI); i++ {
		if isNullString(r.Site.URI[i]) {
			log.Printf("[%v] => site uri value is null!", fileName)
			return true
		}
	}
	if isNullString(r.Site.Host) || isNullString(r.Selector.Find) || isNullString(r.Selector.Child) || isNullArray(r.Selector.Idx) {
		log.Printf("[%v] => selector value is null!", fileName)
		return true
	}
	return false
}

func isNullString(s string) bool {
	if len(strings.Trim(s, " ")) == 0 {
		return true
	}
	return false
}

func isNullArray(a []int) bool {
	if a == nil {
		return true
	}
	if len(a) == 0 {
		return true
	}
	return false
}

// ReloadSQL .
func ReloadSQL() bool {
	var allFile []string
	allFile, err := ccutility.GetAllFileByExt(YamlConf.Info.DBPath, ".sql", allFile)
	if err != nil {
		return false
	}

	if len(allFile) < 1 {
		log.Printf("LoadRules[%v].nil", YamlConf.Info.DBPath)
		return false
	}

	var ret bool
	for _, v := range allFile {
		ret = dbhelper.DBHelper.Rebuild(v)
		log.Printf("Rebuild[%v] => %v", v, ret)
		if ret == false {
			return false
		}
	}
	return true
}
