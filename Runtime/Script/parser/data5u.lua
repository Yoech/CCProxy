--[[
----------------------------------
parser for data5u.com
------------------------------------
 Cool.Cat@2019-07-24 19:00:42
------------------------------------
]]
package.path = package.path .. ";.\\Runtime\\Script\\?.lua;.//Runtime//Script//?.lua"

require("common")

data5u = {}
data5u.slot = "ABCDEFGHIZ"

function data5u.parse(protocol, ip, port)
    --log("data5u.parse[" .. protocol .. "://" .. ip .. ":" .. port .. "]...")

    protocol = string.gsub(protocol, "<li>", "")
    protocol = string.lower(string.gsub(protocol, "</li>", ""))

    if protocol ~= "http" and protocol ~= "https" then
        log("protocol[" .. protocol .. "].error!!!")
        return "", "", ""
    end

    ip = string.gsub(ip, "<li>", "")
    ip = string.gsub(ip, "</li>", "")

    _, t1 = string.find(port, "<li class=\"port ")
    t2, _ = string.find(port, "\">")
    tag = string.sub(port, t1 + 1, t2 - 1)
    port = data5u.decode(tag)
    log("data5u.parse[" .. protocol .. "://" .. ip .. ":" .. port .. "].Done!")

    return port, ip, protocol
end

function data5u.decode(tag)
    local p = ""
    for i = 1, string.len(tag) do
        c = string.sub(tag, i, i)
        x, y = string.find(data5u.slot, c)
        p = p .. x - 1
    end
    return tonumber(p) / 8
end