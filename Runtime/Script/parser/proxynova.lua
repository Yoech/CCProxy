--[[
----------------------------------
parser for proxynova.com
------------------------------------
 Cool.Cat@2020-07-19 16:42:42
------------------------------------
]]
package.path = package.path .. ";.\\Runtime\\Script\\?.lua;.//Runtime//Script//?.lua"

require("common")

proxynova = {}

-- <div class="progress-bar" data-value="27.9464275" title="1317"></div><small>1317 ms</small>
-- <abbr title="101.4.136.34"><script>document.write('101.4.136.34');</script></abbr>
-- 3128

function proxynova.parse(protocol, ip, port)
    --log("proxynova.parse[" .. protocol .. "://" .. ip .. ":" .. port .. "]...")

    --ip = string.match(ip, "<abbr title=\"(%d+%.%d+%.%d+%.%d+)\">")
    ip = string.match(ip, "\'(%d+%.%d+%.%d+%.%d+)\'")
    port = string.match(port, "(%d+)")
    if ip == nil or port == nil then
        return "", "", ""
    end
    --log("proxynova.parse[" .. protocol .. "://" .. ip or "ip" .. ":" .. port or "port" .. "].Done!")

    return port, ip, "http"
end
