--[[
----------------------------------
common func's
------------------------------------
 Cool.Cat@2019-07-24 19:03:08
------------------------------------
]]
lfs = require("lfs")

DEBUG_INFO = function(level)
    local pInfo = debug.getinfo(level, "Sln")
    if pInfo ~= nil then
        return pInfo.source .. "[" .. pInfo.name .. "]" .. "(" .. pInfo.currentline .. ")"
    end
end

log = function(...)
    print(DEBUG_INFO(3) .. " ==> " .. string.format(...))
end

function log_r (t)
    local print_r_cache = {}
    local function sub_print_r(t, indent)
        if (print_r_cache[tostring(t)]) then
            log(indent .. "*" .. tostring(t))
        else
            print_r_cache[tostring(t)] = true
            if (type(t) == "table") then
                for pos, val in pairs(t) do
                    if (type(val) == "table") then
                        log(indent .. "[" .. pos .. "] => " .. tostring(val) .. " {")
                        sub_print_r(val, indent .. string.rep(" ", string.len(pos) + 8))
                        log(indent .. string.rep(" ", string.len(pos) + 6) .. "}")
                    elseif (type(val) == "string") then
                        log(indent .. "[" .. pos .. '] => "' .. val .. '"')
                    else
                        log(indent .. "[" .. pos .. "] => " .. tostring(val))
                    end
                end
            else
                log(indent .. tostring(t))
            end
        end
    end
    if (type(t) == "table") then
        log(tostring(t) .. " {")
        sub_print_r(t, "  ")
        log("}")
    else
        sub_print_r(t, "  ")
    end
end

function RevFolder(rootpath, suffix, pathes, ignoreZeroSize)
    pathes = pathes or {}

    ret, files, iter = pcall(lfs.dir, rootpath)
    if ret == false then
        log("RevFolder.ret[" .. tostring(ret) .. "]")
        return pathes
    end

    local ll = string.len(suffix)

    for entry in files, iter do
        local next = false
        if entry ~= '.' and entry ~= '..' then
            local path = rootpath .. '\\' .. entry
            local attr = lfs.attributes(path)
            if attr == nil then
                next = true
            end
            if next == false then
                if attr.mode == 'directory' then
                    RevFolder(path, suffix, pathes, ignoreZeroSize)
                else
                    if (not ignoreZeroSize) or (ignoreZeroSize and attr["size"] > 0) then
                        if ll > 0 then
                            if string.lower(string.sub(path, string.len(path) - ll + 1)) == string.lower(suffix) then
                                table.insert(pathes, path)
                            end
                        end
                    end
                end
            end
        end
        next = false
    end
    return pathes
end
