<p align="center"><img src="https://raw.githubusercontent.com/Yoech/CCProxy/master/CCProxy.png" width="449" height="174"/></p>

## 　　　　CCProxy: A HTTP/HTTPS crawler based on RESTFul  for Golang

[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php) [![Language](https://img.shields.io/badge/Language-Go-blue.svg)](https://golang.org/) [![GoDoc](https://godoc.org/github.com/Yoech/CCProxy?status.svg)](https://godoc.org/github.com/Yoech/CCProxy) [![Go Report Card](https://goreportcard.com/badge/github.com/Yoech/CCProxy)](https://goreportcard.com/report/github.com/Yoech/CCProxy) [![Build Status](https://travis-ci.org/Yoech/CCProxy.svg?branch=master)](https://travis-ci.org/Yoech/CCProxy) [![Coverage Status](https://coveralls.io/repos/github/Yoech/CCProxy/badge.svg?branch=master)](https://coveralls.io/github/Yoech/CCProxy?branch=master) [![codecov](https://codecov.io/gh/Yoech/CCProxy/branch/master/graph/badge.svg)](https://codecov.io/gh/Yoech/CCProxy) [![CircleCI](https://circleci.com/gh/Yoech/CCProxy/tree/master.svg?style=svg)](https://circleci.com/gh/Yoech/CCProxy/tree/master) [![CircleCI Status](https://circleci.com/gh/Yoech/CCProxy.svg?style=shield)](https://circleci.com/gh/Yoech/CCProxy) [![Gitter](https://badges.gitter.im/CCProxy/community.svg)](https://gitter.im/CCProxy/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge) [![GitHub forks](https://img.shields.io/github/forks/Yoech/CCProxy?style=social)](https://github.com/Yoech/CCProxy/network) [![GitHub stars](https://img.shields.io/github/stars/Yoech/CCProxy?style=social)](https://github.com/Yoech/CCProxy/stargazers)


***It provides the following functions:***
>   Lua script base on [gopher-lua](https://github.com/yuin/gopher-lua "gopher-lua")  
Data-cacheing base on [Redigo](https://github.com/gomodule/redigo "Redigo")  
Data-landing base on [MySQL](https://github.com/go-sql-driver/mysql "MySQL")  
Goquery base on [Goquery](https://github.com/PuerkitoBio/goquery "Goquery")  
Configuration files base on [Yaml.v2](https://gopkg.in/yaml.v2 "Yaml.v2")

---

### Content
- [design principle](#design-principle)
- [restful api](#restful-api)
- [installation](#installation)
- [configuration](#configuration)
- [simple rules without lua script](#simple-rules-without-lua-script)
- [complex rules with lua script](#complex-rules-with-lua-script)


### Design principle
----------------------------------------------------------------
- It can extend business logic through Lua scripts (for example, some URI proxy fields have been encrypted, we use Lua to write decryption code, and can be loaded flexibly).

- Provide RESTFul interfaces for other businesses

- Providing temporary data caches to improve switching efficiency

- Providing data persistence through MySql

- Providing business data configuration through Yaml

- Use user-friendly Go API


### Restful API
----------------------------------------------------------------
|  URI           | Parameters    | Type           | Return         | Description    |
| :------------: | :------------:| :------------: | :------------: | :------------: |
|  EXISTS        |      k        |     string     |    json array  | Return value if the giving key exists
|  GET           |      m        |       int      |    json array  | Maximum data superscript
|                |      n        |       int      |                | Minimum data subscript
|                |      c        |       int      |                | Array count limit.if zero,return all of them.
|  PUT           |      k        |     string     |    true/false  | Put a proxy into the temporary cache
|  DEL           |      k        |     string     |    true/false  | Delete a proxy from temporary cache
|  RELOAD        |      -        |        -       |    true/false  | Reload Yaml Rules
|  SIZE          |      -        |        -       |       int      | Return the number of temporary proxy

> <font color="red">API requests are always with protocol / v1</font>  
>.e.g:

>- return the number of temporary proxy.
>- http://localhost:8888/v1/exists?k=http://191.96.42.106:3129  
>   - =>  
>       {
	"Score": 110,
	"TTL": 2,
	"IP": "191.96.42.106",
	"Port": "3129",
	"Type": "http",
	"Fmt": "http://191.96.42.106:3129",
	"Addr": "Host1Plus数据中心美国",
	"CTime": "2019-08-07 18:59:54",
	"UTime": "2019-08-09 01:55:03"
}
![](https://raw.githubusercontent.com/Yoech/CCProxy/master/exists.jpg)

>- return Score=[110,100] and 2 proxy records.
>- http://localhost:8888/v1/get?m=110&n=100&c=2
>   - =>
>       {
	"ret": [{
			"Score": 110,
			"TTL": 1,
			"IP": "47.52.29.184",
			"Port": "3128",
			"Type": "https",
			"Fmt": "https://47.52.29.184:3128",
			"Addr": "阿里云香港",
			"CTime": "2019-07-30 09:26:53",
			"UTime": "2019-08-01 18:04:33"
		},
		{
			"Score": 110,
			"TTL": 2,
			"IP": "212.64.51.13",
			"Port": "8888",
			"Type": "https",
			"Fmt": "https://212.64.51.13:8888",
			"Addr": " CZ88.NET荷兰",
			"CTime": "2019-07-30 04:48:37",
			"UTime": "2019-08-01 18:04:34"
		}
	]
}
![](https://raw.githubusercontent.com/Yoech/CCProxy/master/get.jpg)

>- return the number of temporary proxy.
>- http://localhost:8888/v1/size  
>   - =>  
>       - 56


### Installation
----------------------------------------------------------------
```go
go get github.com/Yoech/CCProxy

cd $GOPATH/github.com/Yoech/CCProxy

go build

./CCProxy
```


### Configuration
----------------------------------------------------------------
- Runtime configuration.  
ip library/crawler rules/redis/db and agents.

```go
$PATH$/Runtime/Config/Conf.yaml
```

```yaml
api:
  listen: :8888                       # API listening address
  version: /v1                        # API Version
  
info:
  qqwry: QQWry.dat                    # IP Address Library 
  rules: ./Rules/                     # Crawler rules folder

mysql:				      # MySQL configuration
  addr: 127.0.0.1
  port: 3306
  db: ccspider
  user: root
  pass: root
  charset: utf8
  maxconn: 2000
  idleconn: 1000
  
redis:				      # Redis configuration
  addr: 127.0.0.1
  port: 6379
  db: 0
  pass: root
  prefix: cache
  maxconn: 2048
  idleconn: 1024
  idletimeout: 5
  
# User-Agent random array.
# We can use many different agents, just add them below it.
agents: [
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.1",
  "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.12",
  "Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.12",
  "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; 360SE)",
  "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.2",
  "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; The World)",
  "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.51",
  "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Maxthon 2.1)",
  "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.51",
  "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 1.0.3705; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36",
  "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 GTB6 (.NET CLR 3.5.30729)",
  "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
  "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0",
  "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
  "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36",
  "Mozilla/5.0 (Windows NT 5.1; rv:44.0) Gecko/20100101 Firefox/44.0",
  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4",
  "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.813.0 Safari/535.1",
  "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0",
  "Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0",
  "Mozilla/5.0 (Windows NT 6.1; rv:34.0) Gecko/20100101 Firefox/34.0",
  "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:28.0) Gecko/20100101 Firefox/28.0",
  "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.34 (KHTML, like Gecko) Qt/4.8.2",
  "Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1",
  "Mozilla/3.0 (compatible; Indy Library)",
  "Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0",
  "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.813.0 Safari/535.1",
  "Mozilla/5.0 (Windows NT 6.3; WOW64; ReadSharp/6.3.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1736.2 Safari/537.36 OPR/20.0.1380.1",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36",
  "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; MRA 4.4 (build 01334); .NET CLR 1.1.4322)",
  "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208",
  "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/29.0.1547.76 Safari/537.36",
  "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.27+ (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27",
  "Mozilla/5.0 (Windows NT 5.2; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0/puffin",
  "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E; MSN OptimizedIE8;NLNL)",
  "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.2; WOW64; Trident/6.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; InfoPath.3 GD",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3; rv:11.0) like Gecko)",
  "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; en-US; .NET CLR 1.0.3328)",
  "Mozilla/5.0 (Windows NT x.y; rv:10.0) Gecko/20100101 Firefox/10.0",
  "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)",
  "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)",
  "Mozilla/37.0.2 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)",
  "Mozilla/5.0 (compatible; SiteExplorer/1.1b; +http://siteexplorer.info/Backlink-Checker-Spider/)",
  "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727)",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.60 Safari/537.1",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36 QQBrowser/3.8.3858.400",
  "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36",
  "Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)",
  "Mozilla/5.0 (compatible; heritrix/3.1.1 +http://www.baidu.com)",
]

```


### Simple rules without lua script
----------------------------------------------------------------
- e.g: 
```go
$PATH$/Runtime/Rules/xicidaili.yaml
```

```yaml
site:
  uri: http://www.xicidaili.com/nn/		          # Target Uri
  host: www.xicidaili.com			          # Target Host

selector:
  find: "#ip_list tbody tr"			          # Dom element selector
  child: td
  idx: [1,2,5]					          # children element index
```


### Complex rules with lua script
----------------------------------------------------------------
- e.g:
```go
$PATH$/Runtime/Rules/data5u.yaml
```

```yaml
site:
  uri: http://www.data5u.com/                               # Target Uri
  host: www.data5u.com                                      # Target Host
  script: ./Script/parser/data5u.lua                        # lua script to be loaded
  func: data5u.parse                                        # lua function to be executed

selector:
  find: .wlist ul li ul                                     # Dom element selector
  child: span
  idx: [0,1,3]                                              # children element index
```

- Then we try to decrypt the encrypted fields passed in the Lua script.just like the following Lua script:

```go
$PATH$/Runtime/Script/parser/data5u.lua
```

```lua
--[[
----------------------------------
 parser for data5u.com
------------------------------------
 Cool.Cat@2019-07-24 19:00:42
------------------------------------
]]
package.path = package.path .. ";.\\Script\\?.lua;.//Script//?.lua"

require("common")

data5u = {}
data5u.slot = "ABCDEFGHIZ"

function data5u.parse(protocol, ip, port)
    --log("data5u.parse[" .. protocol .. "://" .. ip .. ":" .. port .. "]...")

    protocol = string.gsub(protocol, "<li>", "")
    protocol = string.lower(string.gsub(protocol, "</li>", ""))

    if protocol ~= "http" and protocol ~= "https" then
        log("protocol[" .. protocol .. "].error!!!")
        return "", "", ""
    end

    ip = string.gsub(ip, "<li>", "")
    ip = string.gsub(ip, "</li>", "")

    _, t1 = string.find(port, "<li class=\"port ")
    t2, _ = string.find(port, "\">")
    tag = string.sub(port, t1 + 1, t2 - 1)
    port = data5u.decode(tag)
    log("data5u.parse[" .. protocol .. "://" .. ip .. ":" .. port .. "].Done!")

    return port, ip, protocol
end

function data5u.decode(tag)
    local p = ""
    for i = 1, string.len(tag) do
        c = string.sub(tag, i, i)
        x, y = string.find(data5u.slot, c)
        p = p .. x - 1
    end
    return tonumber(p) / 8
end
```
