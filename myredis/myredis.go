package myredis

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"log"
	"net/textproto"
	"strconv"
	"strings"
	"sync"
	"time"
)

type tagStats struct {
	CountSucc int64
	CountFail int64
	metux     *sync.RWMutex
}

// TagCacheInfo .
type TagCacheInfo struct {
	Cmd  string
	Args []interface{}
}

// TagRedisPoolInfo .
type TagRedisPoolInfo struct {
	Addr        string        `yaml:"addr"`
	Port        int           `yaml:"port"`
	DB          int           `yaml:"db"`
	Pass        string        `yaml:"pass"`
	Prefix      string        `yaml:"prefix"`
	MaxConn     int           `yaml:"maxconn"`
	IdleConn    int           `yaml:"idleconn"`
	IdleTimeout time.Duration `yaml:"idletimeout"`

	Fmt   strings.Builder
	Stats *tagStats
	Pool  *redis.Pool
}

// NewPool .
func NewPool(addr string, port int, database int, password string, prefix string, MaxActive int, MaxIdle int, IdleTimeout time.Duration) *TagRedisPoolInfo {
	p := &TagRedisPoolInfo{
		Addr:        addr,
		Port:        port,
		DB:          database,
		Pass:        password,
		Prefix:      prefix,
		Stats:       &tagStats{0, 0, new(sync.RWMutex)},
		MaxConn:     MaxActive,
		IdleConn:    MaxIdle,
		IdleTimeout: IdleTimeout,
	}

	p.Fmt.Reset()
	p.Fmt.WriteString(p.Addr + ":" + strconv.Itoa(p.Port))

	newPool(p)

	reply, err := p.Exec("SELECT", p.DB)
	if err != nil {
		log.Printf("SELECT %v.err[%v]", p.DB, err)
		return nil
	}
	log.Printf("SELECT %v => %v", p.DB, reply)

	return p
}

func newPool(p *TagRedisPoolInfo) {
	p.Pool = &redis.Pool{
		MaxIdle:     p.IdleConn,
		MaxActive:   p.MaxConn,
		IdleTimeout: p.IdleTimeout,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", p.Fmt.String())
			if err != nil {
				return nil, err
			}
			if len(p.Pass) > 0 {
				if _, err := c.Do("AUTH", p.Pass); err != nil {
					c.Close()
					return nil, err
				}
			}
			if _, err := c.Do("SELECT", p.DB); err != nil {
				c.Close()
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

// Exec .
func (p *TagRedisPoolInfo) Exec(cmd string, args ...interface{}) (interface{}, error) {
	if p == nil || p.Pool == nil || cmd == "" {
		return nil, nil
	}
	c := p.Pool.Get()
	if c.Err() != nil {
		return nil, c.Err()
	}
	defer c.Close()

	var err error
	var reply interface{}
	if reply, err = c.Do(cmd, args...); err != nil {
		p.Stats.metux.Lock()
		p.Stats.CountFail++
		p.Stats.metux.Unlock()
		log.Printf("args=%v", args)
		log.Printf("Exec[%v '%v'].err[%v][%v]", cmd, strings.Trim(fmt.Sprint(args...), "[]"), err, p.Stats.CountFail)
		return nil, err
	}
	p.Stats.metux.Lock()
	p.Stats.CountSucc++
	p.Stats.metux.Unlock()
	//log.Printf("[%v]%v.[%v]", cmd, args, p.Stats.CountSucc)
	return reply, nil
}

// ExecMulti .
func (p *TagRedisPoolInfo) ExecMulti(cmd []TagCacheInfo) (interface{}, error) {
	var total = len(cmd)
	if p == nil || p.Pool == nil || total < 1 {
		return nil, nil
	}
	c := p.Pool.Get()
	if c == nil {
		return nil, nil
	}
	defer c.Close()

	var err error
	var reply interface{}

	var pipeline textproto.Pipeline
	id := pipeline.Next()

	pipeline.StartRequest(id)

	if err = c.Send("MULTI"); err != nil {
		log.Printf("ExecMulti.Send.MULTI.err[%v]", err)
		return nil, nil
	}

	for k, v := range cmd {
		if err = c.Send(v.Cmd, v.Args...); err != nil {
			log.Printf("ExecMulti.Send.cmd[%v][%v].err[%v]", k, v, err)
			//return nil, nil
		}
	}

	pipeline.EndRequest(id)

	pipeline.StartResponse(id)
	reply, err = c.Do("EXEC")
	pipeline.EndResponse(id)

	return reply, err
}

// Status .
func (p *TagRedisPoolInfo) Status() {
	if p == nil || p.Pool == nil {
		return
	}

	p.Stats.metux.RLock()
	log.Printf("----------------------------------")
	log.Printf("\t\t\tRedis Execute Status")
	log.Printf("+--------------------------------+")
	log.Printf("CountSucc = %v", p.Stats.CountSucc)
	log.Printf("CountFail = %v", p.Stats.CountFail)
	log.Printf("+--------------------------------+")
	p.Stats.metux.RUnlock()
}
