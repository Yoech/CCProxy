package cclua

import (
	"errors"
	"fmt"
	lua "github.com/yuin/gopher-lua"
	lfs "layeh.com/gopher-lfs"
	"log"
	"strings"
)

// CustomMoudleName .
const CustomMoudleName = "CCMoudle"

// LuaPool .
var LuaPool = &TagLuaPoolInfo{
	data: make([]*TagLuaInfo, 0, 4),
}

// TagLuaInfo .
type TagLuaInfo struct {
	pState *lua.LState
	pFile  string
}

// NewLua .
func newLua() *TagLuaInfo {
	p := &TagLuaInfo{pState: lua.NewState(), pFile: ""}

	// preload our modules
	p.pState.PreloadModule(CustomMoudleName, loadModule)
	lfs.Preload(p.pState)

	return p
}

// Run .
func (p *TagLuaInfo) Run(file string) (err error) {
	if len(file) == 0 {
		return errors.New("Can't run without script files")
	}

	if err = p.pState.DoFile(file); err != nil {
		log.Printf("err[%v]", err)
		return nil
	}

	p.pFile = file
	return err
}

// Close .
func (p *TagLuaInfo) Close() {
	if p.pState == nil {
		return
	}
	p.pState.Close()
}

func loadModule(L *lua.LState) int {
	mod := L.SetFuncs(L.NewTable(), exports)
	L.SetField(mod, "name", lua.LString(CustomMoudleName))
	L.Push(mod)
	return 1
}

var exports = map[string]lua.LGFunction{
	"goFunc": goFunc,
}

func goFunc(L *lua.LState) int {
	a := L.ToInt(1)
	log.Printf("call from lua , a=%v", a)
	return 0
}

// CallLuaFunc .
func (p *TagLuaInfo) CallLuaFunc(funcName string, nRet int, args ...lua.LValue) (ret []lua.LValue, err error) {
	if p.pState == nil {
		return nil, errors.New("gLua is nil")
	}

	var arr []string
	var fun = p.pState.GetGlobal(funcName)
	if fun == lua.LNil {
		arr = strings.Split(funcName, ".")
		if len(arr) < 2 {
			arr = strings.Split(funcName, ":")
			if len(arr) >= 2 {
				result := make([]lua.LValue, len(args)+1)
				result[0] = lua.LString("self")
				for i := 0; i < len(args); i++ {
					result[i+1] = args[i]
				}
				args = result
			} else {
				return nil, fmt.Errorf("func[%v].no-exists", funcName)
			}
		}

		cls := p.pState.GetGlobal(arr[0])
		if cls == nil {
			return nil, fmt.Errorf("className[%v].no-exists", arr[0])
		}
		fun = p.pState.GetField(cls, arr[1])
		if fun == nil {
			return nil, fmt.Errorf("funcName[%v].no-exists", arr[1])
		}
	}

	if err = p.pState.CallByParam(lua.P{
		Fn:      fun,
		NRet:    nRet,
		Protect: true,
	}, args...); err != nil {
		log.Printf("CallLuaFunc.err[%v]", err)
		return nil, err
	}

	var rVals = make([]lua.LValue, 0)

	for i := 0; i < nRet; i++ {
		rVals = append(rVals, p.pState.Get((i+1)*-1))
	}

	// clean stack
	p.pState.Pop(nRet)

	return rVals, nil
}
