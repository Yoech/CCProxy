package cclua

import (
	"sync"
)

// TagLuaPoolInfo .
type TagLuaPoolInfo struct {
	m    sync.Mutex
	data []*TagLuaInfo
}

// Get .
func (p *TagLuaPoolInfo) Get() *TagLuaInfo {
	p.m.Lock()
	defer p.m.Unlock()
	n := len(p.data)
	if n == 0 {
		return newLua()
	}
	x := p.data[n-1]
	p.data = p.data[0 : n-1]
	return x
}

// Put .
func (p *TagLuaPoolInfo) Put(L *TagLuaInfo) {
	p.m.Lock()
	defer p.m.Unlock()
	p.data = append(p.data, L)
}

// Close .
func (p *TagLuaPoolInfo) Close() {
	for _, L := range p.data {
		L.Close()
	}
}

// Count .
func (p *TagLuaPoolInfo) Count() int {
	p.m.Lock()
	defer p.m.Unlock()
	return len(p.data)
}
