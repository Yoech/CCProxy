package dbhelper

import (
	"github.com/Yoech/CCProxy/config"
	"io/ioutil"
	"log"
	"strings"
	"sync"
)

// TagDBHelperInfo .
type TagDBHelperInfo struct {
	mapCate  map[string]int64
	mapTags  map[string]int64
	lockCate *sync.RWMutex
	lockTags *sync.RWMutex
}

// DBHelper .
var DBHelper *TagDBHelperInfo

func init() {
	if DBHelper != nil {
		return
	}

	DBHelper = &TagDBHelperInfo{
		make(map[string]int64, 0),
		make(map[string]int64, 0),
		new(sync.RWMutex),
		new(sync.RWMutex),
	}
}

// Rebuild .
func (p *TagDBHelperInfo) Rebuild(sqlpath string) bool {
	if config.MySQL == nil {
		log.Printf("Rebuild.MySQL.nil")
		return false
	}

	var err error

	if sqlpath == "" {
		return false
	}

	sqlFile, err := ioutil.ReadFile(sqlpath)
	if err != nil {
		log.Printf("Rebuild.ReadFile[%v].err[%v]", sqlpath, err)
		return false
	}

	sqlData := string(sqlFile)

	// 'DELIMITER' is a client-side feature.The server-side doesn't recognize this character.
	// so we replace 'DELIMITER' to empty string.and replace '$$' to ';'
	// Cool.Cat@2019-08-05 20:12:47
	sqlData = strings.Replace(strings.Replace(strings.Replace(sqlData, "DELIMITER $$", "", -1), "DELIMITER ;", "", -1), "$$", ";", -1)

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	_, err = tx.Exec(sqlData)
	if err != nil {
		log.Printf("Rebuild.Exec.err[%v]", err)
		return false
	}

	if err = tx.Commit(); err != nil {
		log.Printf("Rebuild.Commit.err[%v]", err)
		return false
	}

	return true
}
