package ccapi

import (
	"github.com/Yoech/CCProxy/ccyaml"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMain(m *testing.M) {
	log.Printf("TestMain...begin")
	m.Run()
	log.Printf("TestMain...End")
}

func TestRun(t *testing.T) {
	api := &ccyaml.TagYamlAPIInfo{Listen: ":9999", Version: "/v1"}
	go Run(*api)
}

// TestHandlerGet .
func TestHandlerGet(t *testing.T) {
	t.Log("TestHandlerGet")
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "", nil)
	HandlerGet(w, r)
}

// TestHandlerPut .
func TestHandlerPut(t *testing.T) {
	t.Log("TestHandlerPut")
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "", nil)
	HandlerPut(w, r)
}

// TestHandlerDel .
func TestHandlerDel(t *testing.T) {
	t.Log("TestHandlerDel")
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "", nil)
	HandlerDel(w, r)
}

// TestHandlerReload .
func TestHandlerReload(t *testing.T) {
	t.Log("TestHandlerReload")
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "", nil)
	HandlerReload(w, r)
}

// TestHandlerSize .
func TestHandlerSize(t *testing.T) {
	t.Log("TestHandlerSize")
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "", nil)
	HandlerSize(w, r)
}
