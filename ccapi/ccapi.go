package ccapi

import (
	"encoding/json"
	"github.com/Yoech/CCProxy/ccproxy"
	"github.com/Yoech/CCProxy/ccyaml"
	"log"
	"net/http"
	"strconv"
)

// Run .
func Run(api ccyaml.TagYamlAPIInfo) {
	mux := http.NewServeMux()
	mux.HandleFunc(api.Version+"/exists", HandlerExists)
	mux.HandleFunc(api.Version+"/get", HandlerGet)
	mux.HandleFunc(api.Version+"/put", HandlerPut)
	mux.HandleFunc(api.Version+"/del", HandlerDel)
	mux.HandleFunc(api.Version+"/reload", HandlerReload)
	mux.HandleFunc(api.Version+"/size", HandlerSize)
	log.Println("Starting server", api.Listen)
	http.ListenAndServe(api.Listen, mux)
}

// HandlerExists .
func HandlerExists(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	v := r.URL.Query()
	k := v.Get("k")
	p := ccproxy.Exists(k)
	if p == nil {
		return
	}
	b, err := json.Marshal(p)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

// HandlerGet .
func HandlerGet(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	v := r.URL.Query()
	m, _ := strconv.Atoi(v.Get("m"))
	n, _ := strconv.Atoi(v.Get("n"))
	c, _ := strconv.Atoi(v.Get("c"))
	p := ccproxy.Get(m, n, c)
	if p == "" {
		return
	}
	b, err := json.Marshal(p)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

// HandlerPut .
func HandlerPut(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	v := r.URL.Query()
	k := v.Get("k")
	p := ccproxy.Put(k)
	b, err := json.Marshal(p)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

// HandlerDel .
func HandlerDel(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	v := r.URL.Query()
	k := v.Get("k")
	p := ccproxy.Put(k)
	b, err := json.Marshal(p)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

// HandlerReload .
func HandlerReload(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	total := ccyaml.ReloadRules()
	b, err := json.Marshal(total)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

// HandlerSize .
func HandlerSize(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	total := ccproxy.Size()
	b, err := json.Marshal(total)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}
