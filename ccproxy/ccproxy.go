package ccproxy

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/Yoech/CCProxy/cclua"
	"github.com/Yoech/CCProxy/ccutility"
	"github.com/Yoech/CCProxy/ccyaml"
	"github.com/Yoech/CCProxy/config"
	"github.com/Yoech/CCProxy/myredis"
	"github.com/gomodule/redigo/redis"
	lua "github.com/yuin/gopher-lua"
	"log"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

var gCmd []myredis.TagCacheInfo
var gTsql [2][]string
var gAlive = 0
var gStatus = make(chan int)
var lockProxy = new(sync.RWMutex)

// TagProxyIPInfo .
type TagProxyIPInfo struct {
	Score int64
	TTL   int64
	IP    string
	Port  string
	Type  string
	Fmt   string
	Addr  string
	CTime string
	UTime string
}

// ProxyData .
var ProxyData = make(map[string]*TagProxyIPInfo)

// Import2CacheFromDB .
func Import2CacheFromDB() {
	lockProxy.Lock()
	defer func() {
		lockProxy.Unlock()
	}()

	result, err := config.MySQL.Query("select ProxyIp,ProxyData from ccproxy;")
	if err != nil {
		log.Printf("Import2CacheFromDB.err[%v]", err)
		return
	}

	total := 0
	for _, v := range result {
		pAddr := v["ProxyIp"].(string)
		pData := v["ProxyData"].(string)

		p := &TagProxyIPInfo{}
		if err = json.Unmarshal([]byte(pData), &p); err != nil {
			log.Printf("Import2CacheFromDB.Unmarshal[%v].err[%v]", pAddr, err)
			break
		}

		ProxyData[p.Fmt] = p
		update(p, false)
		total++
	}
	flush()
	log.Printf("Import2CacheFromDB.total[%v]", total)
}

// LoadFromCache .
func LoadFromCache() {
	lockProxy.Lock()
	defer lockProxy.Unlock()

	if len(ProxyData) > 0 {
		return
	}

	cursor := "0"
	for {
		ret, err := redis.Values(config.RedisPool.Exec("HSCAN", config.GlobalURIProxyHash, cursor, "COUNT", "3"))
		if err != nil {
			break
		}

		cursor, err = redis.String(ret[0], err)
		if err != nil {
			break
		}
		val, err := redis.Strings(ret[1], err)
		if err != nil {
			break
		}

		for i := 0; i < len(val)/2; i++ {
			s := val[i*2+1]
			p := &TagProxyIPInfo{}
			if err = json.Unmarshal([]byte(s), &p); err != nil {
				break
			}
			ProxyData[p.Fmt] = p
		}

		if cursor == "0" {
			break
		}
	}
	log.Printf("LoadFromCache.ProxyData[%v]", len(ProxyData))
}

// Run crawl the target uri and check-alive in the loop.
func Run(bTestMode bool) {
	var wg = &sync.WaitGroup{}
	wg.Add(1)
	if bTestMode {
		go func(WG *sync.WaitGroup) {
			defer wg.Done()
			Worker()
		}(wg)
	} else {
		ccutility.WorkerTimer(2, 4, func() {
			Worker()
		})
	}
	wg.Wait()
}

// Worker .
func Worker() {
	queryAllProxy()
	checkAlive()
	flush()
	log.Printf("CCProxy.Run....Done!")
}

func template(wg *sync.WaitGroup, conf *ccyaml.TagYamlConfInfo, rules *ccyaml.TagYamlRulesInfo, idx int) {
	defer wg.Done()

	var err error
	var rsp *http.Response
	var dom *goquery.Document

	req, _ := http.NewRequest("GET", rules.Site.URI[idx], nil)

	req.Header.Set("authority", rules.Site.Host)
	req.Header.Set("method", "GET")
	req.Header.Set("path", "/")
	req.Header.Set("scheme", "https")

	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
	//// DON'T ENABLE THIS!!!
	//req.Header.Set("Accept-Encoding", "gzip, deflate")
	req.Header.Set("Accept-Language", "en,zh-CN;q=0.9,zh;q=0.8")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Cache-Control", "max-age=0")
	//req.Header.Set("Cookie", "UM_distinctid=16bbbd56807be4-0a12bf3d5145a4-e343166-1fa400-16bbbd56808898; JSESSIONID=539146F5F6D79CF49E9553F064E28923; CNZZDATA1260383977=1246750663-1562219393-%7C1563869788; Hm_lvt_3406180e5d656c4789c6c08b08bf68c2=1563570126,1563570149,1563571326,1563873567; Hm_lpvt_3406180e5d656c4789c6c08b08bf68c2=1563873630")
	req.Header.Set("Host", rules.Site.Host)
	req.Header.Set("Referer", rules.Site.Host)
	req.Header.Set("Upgrade-Insecure-Requests", "1")

	if rules.Site.Agents == nil {
		req.Header.Set("User-Agent", RandomAgent(conf.Agents))
	} else {
		req.Header.Set("User-Agent", RandomAgent(rules.Site.Agents))
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	c := &http.Client{Transport: tr}

	rsp, err = c.Do(req)
	if err != nil {
		log.Printf("err[%v]", err)
		return
	}

	defer rsp.Body.Close()

	dom, err = goquery.NewDocumentFromReader(rsp.Body)
	if err != nil {
		return
	}

	var ret = make([]string, 0)
	dom.Find(rules.Selector.Find).Each(func(i int, context *goquery.Selection) {
		child := context.Find(rules.Selector.Child)
		for j := 0; j < 3; j++ {
			idx := rules.Selector.Idx[j]
			s := child.Eq(idx)
			if s != nil {
				h, _ := s.Html()
				//log.Printf("h=%v", h)
				ret = append(ret, h)
				//ret = append(ret, child.Eq(idx).Text())
			}
		}
	})

	count := len(ret) / 3
	log.Printf("Uri[%v][%v] => found %v records.", idx, rules.Site.URI[idx], count)

	// exec custom script when we define it in the yaml file
	if len(rules.Site.Script) > 0 && len(rules.Site.Func) > 0 {
		pLua := cclua.LuaPool.Get()
		defer cclua.LuaPool.Put(pLua)
		if err = pLua.Run(rules.Site.Script); err != nil {
			log.Printf("Run[%v].err[%v]", rules.Site.Script, err)
			return
		}

		for i := 0; i < count; i++ {
			base := i * 3

			// we call lua func to decode ciphertext
			// Cool.Cat@2019-07-28 14:54:26
			var rVals []lua.LValue
			rVals, err = pLua.CallLuaFunc(rules.Site.Func, 3, lua.LString(ret[base+2]), lua.LString(ret[base]), lua.LString(ret[base+1]))
			if err == nil {
				protocol := rVals[0].String()
				ip := rVals[1].String()
				port := rVals[2].String()
				if len(protocol) > 0 && len(ip) > 0 && len(port) > 0 {
					SaveAddr(rVals[1].String(), rVals[2].String(), rVals[0].String())
				}
			}
		}
		return
	}

	for i := 0; i < count; i++ {
		base := i * 3
		SaveAddr(ret[base], ret[base+1], ret[base+2])
	}
}

func queryAllProxy() {
	s := time.Now()

	var wg = &sync.WaitGroup{}
	for _, r := range ccyaml.YamRules {
		for i := 0; i < len(r.Site.URI); i++ {
			wg.Add(1)
			func(WG *sync.WaitGroup, F func(*sync.WaitGroup, *ccyaml.TagYamlConfInfo, *ccyaml.TagYamlRulesInfo, int), C *ccyaml.TagYamlConfInfo, R *ccyaml.TagYamlRulesInfo, I int) {
				F(WG, C, R, I)
			}(wg, template, ccyaml.YamlConf, r, i)
		}
	}
	wg.Wait()

	cost := time.Now().Unix() - s.Unix()

	lockProxy.RLock()
	log.Printf("queryAllProxy.total[%v].cost[%v s]...Done", len(ProxyData), cost)
	lockProxy.RUnlock()
}

func checkAlive() {
	s := time.Now()

	checkCount := 0
	gAlive = 0

	var wg = &sync.WaitGroup{}
	for _, v := range ProxyData {
		wg.Add(1)
		checkCount++
		go func(WG *sync.WaitGroup, V *TagProxyIPInfo) {
			defer WG.Done()
			checkProxyIP(V)
		}(wg, v)
	}
	wg.Wait()

	cost := time.Now().Unix() - s.Unix()

	lockProxy.RLock()
	log.Printf("checkAlive.checkCount[%v/%v].aliveCount[%v].cost[%v s]...Done", checkCount, len(ProxyData), gAlive, cost)
	lockProxy.RUnlock()
}

// SaveAddr .
func SaveAddr(IP string, Port string, Type string) {
	IP = strings.Trim(IP, "\r\n\t ")
	Port = strings.Trim(Port, "\r\n\t ")
	Type = strings.ToLower(strings.Trim(Type, "\r\n\t "))

	if IP == "" || Port == "" {
		return
	}

	if Type == "" {
		Type = "http"
	}

	fmtIP := fmt.Sprintf("%v://%v:%v", Type, IP, Port)

	lockProxy.Lock()
	defer lockProxy.Unlock()
	if _, ok := ProxyData[fmtIP]; ok == true {
		return
	}

	p := &TagProxyIPInfo{IP: IP, Port: Port, Type: Type, Fmt: fmtIP, Addr: "", Score: config.GlobalProxyDefaultScore, TTL: 0, CTime: time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime), UTime: time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime)}
	if ProxyData[fmtIP] != nil {
		log.Printf("SaveAddr.[%v://%v:%v].exists", Type, IP, Port)
		return
	}
	ProxyData[fmtIP] = p

	res := config.QQ.Query(p.IP)
	p.Addr = res.Area + res.Country
	log.Printf("SaveAddr ... %v://%v:%v => %v ... OK", Type, IP, Port, p.Addr)
}

func checkProxyIP(p *TagProxyIPInfo) {
	type tagIPInfo struct {
		Origin string `json:"origin"`
	}

	s := time.Now()
	var proxy *url.URL
	var c *http.Client
	var req *http.Request
	var rsp *http.Response
	var bValid = false
	var dialer *net.Dialer
	var err error
	var proxyURI string
	var js *tagIPInfo
	var buf []byte
	var arr []string

	req, err = http.NewRequest("GET", "https://httpbin.org/ip", nil)
	if err != nil {
		//log.Printf("checkProxyIP.proxyIp[%v].NewRequest.err[%v]", proxyIp, err)
		goto doClear
	}

	// Whether HTTP or HTTPS proxy, all use HTTP proxy
	// otherwise 'first record does not look like a TLS handshake' will be promoted.
	// Cool.Cat@2019-07-29 13:19:55
	//proxy, err = url.Parse(p.Fmt)
	proxyURI = fmt.Sprintf("http://%v:%v", p.IP, p.Port)
	proxy, err = url.Parse(proxyURI)
	if err != nil {
		//log.Printf("checkProxyIP.proxyIp[%v].Parse.err[%v]", proxyIp, err)
		goto doClear
	}

	dialer = &net.Dialer{
		Timeout:   time.Duration(5) * time.Second,
		KeepAlive: time.Duration(5) * time.Second,
	}

	c = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},
			Proxy:               http.ProxyURL(proxy),
			DisableCompression:  true,
			DialContext:         dialer.DialContext,
			TLSHandshakeTimeout: 5 * time.Second,
			// disabled HTTP/2
			TLSNextProto: make(map[string]func(authority string, c *tls.Conn) http.RoundTripper),
		},
		Timeout: time.Duration(5) * time.Second,
	}

	req.Header.Set("Accept-Encoding", "identity")

	rsp, err = c.Do(req)
	if rsp != nil {
		defer rsp.Body.Close()
	}
	if err != nil {
		//log.Printf("checkProxyIP.proxy[%v].Do.err[%v]", p.Fmt, err)
		goto doClear
	}

	if rsp == nil || (rsp != nil && (rsp.StatusCode != 200 || rsp.ContentLength < 1)) {
		goto doClear
	}

	buf = make([]byte, rsp.ContentLength)
	_, _ = rsp.Body.Read(buf)

	js = &tagIPInfo{}
	if err := json.Unmarshal(buf, &js); err != nil {
		//log.Printf("checkProxyIP.Unmarshal.err[%v]", err)
		goto doClear
	}

	arr = strings.Split(js.Origin, ",")
	for _, v := range arr {
		// e.g: "113.87.88.224, 113.87.88.224"
		if strings.Contains(p.Fmt, strings.Trim(v, " ")) == true {
			bValid = true
			lockProxy.Lock()
			gAlive++
			lockProxy.Unlock()
			break
		}
	}
doClear:
	e := time.Now()
	p.TTL = e.Unix() - s.Unix()

	needUpdate(p, bValid)
}

func needUpdate(p *TagProxyIPInfo, bValid bool) {
	if p.Addr == "" {
		res := config.QQ.Query(p.IP)
		p.Addr = res.Area + res.Country
	}

	if p.CTime == "" {
		p.CTime = time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime)
	}

	p.UTime = time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime)

	if bValid == true {
		if p.Score < config.GlobalProxyDefaultScore {
			p.Score = config.GlobalProxyDefaultScore
		} else if p.Score >= config.GlobalProxyDefaultScore && p.Score < config.GlobalProxyMaxScore {
			p.Score++
		}
	} else {
		p.Score--
	}

	update(p, true)
}

func update(p *TagProxyIPInfo, needLock bool) {
	pData, err := json.Marshal(p)
	if err != nil {
		return
	}

	if needLock {
		lockProxy.Lock()
		defer lockProxy.Unlock()
	}

	if p.Score < config.GlobalProxyDefaultScore {
		gCmd = append(gCmd, myredis.TagCacheInfo{Cmd: "HDEL", Args: redis.Args{}.Add(config.GlobalURIProxyHash).Add(p.Fmt)})
		gCmd = append(gCmd, myredis.TagCacheInfo{Cmd: "ZREM", Args: redis.Args{}.Add(config.GlobalURIProxyZset).Add(p.Fmt)})

		gTsql[0] = append(gTsql[0], fmt.Sprintf("'%v',", p.Fmt))

		delete(ProxyData, p.Fmt)
	}

	if p.Score > config.GlobalProxyDefaultScore {
		gCmd = append(gCmd, myredis.TagCacheInfo{Cmd: "ZADD", Args: redis.Args{}.Add(config.GlobalURIProxyZset).Add(p.Score).Add(p.Fmt)})
		gCmd = append(gCmd, myredis.TagCacheInfo{Cmd: "HMSET", Args: redis.Args{}.Add(config.GlobalURIProxyHash).Add(p.Fmt).Add(pData)})

		gTsql[1] = append(gTsql[1], fmt.Sprintf("('%v','%v'),", p.Fmt, string(pData)))

		ProxyData[p.Fmt] = p
	}
}

func flush() {
	RemoveAllCripple(false)
	update2Cache()
	update2DB()
}

// RemoveAllCripple .
func RemoveAllCripple(bAll bool) {
	if bAll {
		config.RedisPool.Exec("DEL", config.GlobalURIProxyHash)
		config.RedisPool.Exec("DEL", config.GlobalURIProxyZset)
		return
	}
	config.RedisPool.Exec("ZREMRANGEBYSCORE", config.GlobalURIProxyZset, "-inf", config.GlobalProxyDefaultScore+1)
}

func update2Cache() {
	log.Printf("update2Cache.cmd.total[%v]", len(gCmd))

	for {
		x := len(gCmd)
		if x < 1 {
			break
		}
		if x > 100 {
			_, err := config.RedisPool.ExecMulti(gCmd[:100])
			if err != nil {
				log.Printf("update2Cache.err[%v].A", err)
			}
			gCmd = gCmd[100:]
		} else {
			_, err := config.RedisPool.ExecMulti(gCmd[0:])
			if err != nil {
				log.Printf("update2Cache.err[%v].B", err)
			}
			gCmd = gCmd[0:0]
			break
		}
	}
}

func update2DB() {
	log.Printf("update2DB.cmd.total[%v]", len(gTsql))

	if len(gTsql) < 1 {
		return
	}

	s := [...]string{
		"delete from ccproxy where ProxyIp in (",
		"insert into ccproxy (ProxyIp,ProxyData) values ",
	}
	e := [...]string{
		");",
		"ON DUPLICATE KEY UPDATE ProxyIp=VALUES(ProxyIp),ProxyData=VALUES(ProxyData);",
	}

	for i := 0; i < 2; i++ {
		for {
			if gTsql[i] == nil {
				break
			}
			x := len(gTsql[i])
			if len(gTsql[i]) < 1 {
				break
			}
			if x > 100 {
				if update2DBwithSlice(i, 100, s[i], e[i]) == false {
					// error handle or ignore it
				}
				gTsql[i] = gTsql[i][100:]
			} else {
				if update2DBwithSlice(i, x, s[i], e[i]) == false {
					// error handle or ignore it
				}
				gTsql[i] = gTsql[i][0:0]
				break
			}
		}
	}
}

func update2DBwithSlice(idx int, nPos int, start string, end string) bool {
	if config.MySQL == nil {
		log.Printf("update2DBwithSlice.MySQL.nil")
		return false
	}

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	var sqlStr strings.Builder
	sqlStr.WriteString(start)

	for _, v := range gTsql[idx][:nPos] {
		sqlStr.WriteString(v)
	}

	sqlStr2 := strings.TrimRight(sqlStr.String(), ",")
	sqlStr2 = fmt.Sprintf("%v %v", sqlStr2, end)

	_, err := tx.Exec(sqlStr2)
	if err != nil {
		log.Printf("update2DBwithSlice.Insert.err[%v]", err)
		return false
	}

	if err = tx.Commit(); err != nil {
		log.Printf("update2DBwithSlice.Commit.err[%v]", err)
		return false
	}
	return true
}

// RandomAgent .
func RandomAgent(agent []string) string {
	length := len(agent)
	if length == 0 {
		return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
	}
	rnd := ccutility.RndNum(0, int64(length))
	return agent[rnd]
}

// Exists .
func Exists(k string) *TagProxyIPInfo {
	if len(k) < 1 {
		return nil
	}

	lockProxy.RLock()
	defer lockProxy.RUnlock()
	if val, ok := ProxyData[k]; ok {
		return val
	}
	return nil
}

// Get .
func Get(m int, n int, c int) string {
	var err error
	var key []interface{}
	var res []interface{}
	if m == 0 {
		m = config.GlobalProxyMaxScore
	}
	key, err = redis.Values(config.RedisPool.Exec("ZREVRANGEBYSCORE", config.GlobalURIProxyZset, strconv.Itoa(m), strconv.Itoa(n)))
	if err != nil || key == nil {
		return ""
	}

	maxNum := len(key)
	if maxNum == 0 {
		return ""
	}

	if maxNum > c && c > 0 {
		maxNum = c
	}

	res, err = redis.Values(config.RedisPool.Exec("HMGET", redis.Args{config.GlobalURIProxyHash}.AddFlat(key[:maxNum])...))
	if err != nil || res == nil {
		return ""
	}

	var s = make([]string, 0)
	b := strings.Builder{}
	b.WriteString("{")
	b.WriteString("\"ret\":")

	for i := 0; i < len(res); i++ {
		if res[i] == nil {
			break
		}
		xx := res[i].([]byte)
		s = append(s, string(xx))
		s = append(s, ",")
	}
	s = s[:len(s)-1]
	b.WriteString(fmt.Sprint(s))
	b.WriteString("}")

	return b.String()
}

// Put .
func Put(k string) bool {
	if len(k) < 1 {
		return false
	}

	var p *TagProxyIPInfo

	Type, IP, Port, Fmt, succ := ccutility.ParseURL(k)
	if succ == false {
		return false
	}

	p = &TagProxyIPInfo{IP: IP, Port: Port, Type: Type, Fmt: Fmt, Addr: "", Score: config.GlobalProxyDefaultScore, TTL: 0, CTime: time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime), UTime: time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime)}

	lockProxy.Lock()
	defer lockProxy.Unlock()
	ProxyData[p.Fmt] = p

	return true
}

// Del .
func Del(k string) bool {
	if len(k) < 1 {
		return false
	}

	_, _, _, Fmt, succ := ccutility.ParseURL(k)
	if succ == false {
		return false
	}

	lockProxy.Lock()
	defer lockProxy.Unlock()
	delete(ProxyData, Fmt)

	return true
}

// Size .
func Size() int {
	lockProxy.RLock()
	defer lockProxy.RUnlock()
	count := len(ProxyData)
	return count
}
