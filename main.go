package main

import (
	"github.com/Yoech/CCProxy/ccapi"
	"github.com/Yoech/CCProxy/cclua"
	"github.com/Yoech/CCProxy/ccproxy"
	"github.com/Yoech/CCProxy/ccutility"
	"github.com/Yoech/CCProxy/ccyaml"
	"github.com/Yoech/CCProxy/config"
	"log"
	"os"
	"strings"
)

func main() {
	// check test mode
	params := os.Args
	var bTestMode = false
	if len(params) > 1 {
		test := os.Args[1]
		if strings.Contains(test, "-test.") {
			bTestMode = true
		}
	}
	log.Printf("bTestMode[%v].params=%v", bTestMode, params)

	// load config and rules from yaml files.
	if ccyaml.LoadConf("./Runtime/Config/Conf.yaml") == true {

		go func() {
			ccapi.Run(ccyaml.YamlConf.API)
		}()

		// u fucked up
		defer func() {
			// shutdown all lua-VM.
			cclua.LuaPool.Close()

			// capture runtime errors here.
			ccutility.CatchError()
		}()

		// clean cache first.
		ccproxy.RemoveAllCripple(true)

		// import records to redis from database.
		ccproxy.Import2CacheFromDB()

		// load proxy data from redis.
		ccproxy.LoadFromCache()

		// Run logic
		ccproxy.Run(bTestMode)
	}

	// show cache status
	config.RedisPool.Status()

	log.Printf("Done!")
	return
}
