package config

import (
	"github.com/Yoech/CCProxy/myredis"
	"github.com/Yoech/CCProxy/mysql"
	"github.com/Yoech/CCProxy/qqwry"
)

// GlobalBaseTime .
const GlobalBaseTime = "2006-01-02 15:04:05"

// GlobalProxyDefaultScore .
const GlobalProxyDefaultScore = 100

// GlobalProxyMaxScore .
const GlobalProxyMaxScore = GlobalProxyDefaultScore + 10

// GlobalURIProxyHash .
const GlobalURIProxyHash = "CCP:P:H"

// GlobalURIProxyZset .
const GlobalURIProxyZset = "CCP:P:ZS"

// QQ .
var QQ *qqwry.QQwry

// RedisPool .
var RedisPool *myredis.TagRedisPoolInfo

// MySQL .
var MySQL *mysql.TagMySQLPoolInfo
