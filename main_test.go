package main

import (
	"fmt"
	"github.com/Yoech/CCProxy/ccutility"
	"github.com/Yoech/CCProxy/qqwry"
	"log"
	"testing"
)

var QQ *qqwry.QQwry

func init() {
	QQ = qqwry.NewPool("./Runtime/QQWry/QQWry.dat")
	if QQ == nil {
		return
	}
	log.Printf("%v", QQ.Version())
}

func TestMain(m *testing.M) {
	log.Printf("TestMain...begin")
	m.Run()
	log.Printf("TestMain...End")
}

func TestHttpsTest(t *testing.T) {
	main()
}

func TestQQwry_Query(t *testing.T) {
	if QQ == nil {
		return
	}

	var res qqwry.ResultQQwry
	var x1, x2, x3, x4 int64

	for i := 0; i < 10; i++ {
		x1 = ccutility.RndNum(1, 255)
		x2 = ccutility.RndNum(1, 255)
		x3 = ccutility.RndNum(1, 255)
		x4 = ccutility.RndNum(1, 255)
		res = QQ.Query(fmt.Sprintf("%d.%d.%d.%d", x1, x2, x3, x4))
		log.Printf("[TestQQwry_Query] %v => %s%s", res.IP, res.Country, res.Area)
	}
}

func BenchmarkQQwry_Query(b *testing.B) {
	if QQ == nil {
		return
	}

	b.StopTimer()

	b.StartTimer()

	var res qqwry.ResultQQwry
	var x1, x2, x3, x4 int64

	for i := 0; i < b.N; i++ {
		x1 = ccutility.RndNum(1, 255)
		x2 = ccutility.RndNum(1, 255)
		x3 = ccutility.RndNum(1, 255)
		x4 = ccutility.RndNum(1, 255)
		res = QQ.Query(fmt.Sprintf("%d.%d.%d.%d", x1, x2, x3, x4))
		log.Printf("[BenchmarkQQwry_Query][%v/%v] %v => %s%s", i, b.N, res.IP, res.Country, res.Area)
	}

	b.ResetTimer()
}
