package ccutility

import (
	"fmt"
	"github.com/Yoech/CCProxy/config"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net"
	"runtime/debug"
	"sync"

	// xxx
	_ "net/http/pprof"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// Round .
func Round(f float64) int {
	return int(math.Floor(f + 0.5))
}

// WorkerTimer .
func WorkerTimer(d1 int64, d2 int64, f func()) {
	go func(min int64, max int64) {
		for {
			f()
			d := RndNum(min, max)
			now := time.Now().In(time.FixedZone("CST", 8*3600))
			next := now.Add(time.Duration(d) * time.Minute)
			t := time.NewTimer(next.Sub(now))
			<-t.C
		}
	}(d1, d2)
}

// TimeDiffNow .
func TimeDiffNow(e string) (diff int64) {
	loc, _ := time.LoadLocation("Local")
	lastUTime, err := time.ParseInLocation(config.GlobalBaseTime, e, loc)
	if err != nil {
		lastUTime = time.Now().In(time.FixedZone("CST", 8*3600))
	}

	diff = time.Now().In(time.FixedZone("CST", 8*3600)).Unix() - lastUTime.Unix()
	return
}

var lockSeed = new(sync.RWMutex)

// GlobalSeed .
var GlobalSeed *rand.Rand

// RndNum .
func RndNum(min, max int64) int64 {
	if min >= max {
		return max
	}

	lockSeed.Lock()
	defer lockSeed.Unlock()
	if GlobalSeed == nil {
		GlobalSeed = rand.New(rand.NewSource(time.Now().UnixNano()))
	}

	if min == 0 {
		min++
		max++
		return GlobalSeed.Int63n(max-min) + min - 1
	}

	return GlobalSeed.Int63n(max-min) + min
}

// GetAllFileByExt .
func GetAllFileByExt(pathname string, ext string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(pathname)
	if err != nil {
		log.Printf("GetAllFileByExt.ReadDir[%v].err[%v]", pathname, err)
		return s, err
	}
	for _, fi := range rd {
		if fi.IsDir() {
			fullDir := pathname + "/" + fi.Name()
			s, err = GetAllFileByExt(fullDir, ext, s)
			if err != nil {
				log.Printf("GetAllFileByExt[%v].err[%v]", fullDir, err)
				return s, err
			}
		} else {
			if strings.HasSuffix(strings.ToLower(fi.Name()), strings.ToLower(ext)) {
				fullName := pathname + "/" + fi.Name()
				s = append(s, fullName)
			}
		}
	}
	return s, nil
}

// ParseURL .
func ParseURL(k string) (Type string, IP string, Port string, Fmt string, succ bool) {
	uri, err := url.Parse(k)
	if err != nil {
		return "", "", "", "", false
	}
	Type = strings.ToLower(uri.Scheme)

	addr := strings.Split(uri.Host, ":")
	length := len(addr)
	switch length {
	case 1:
		{
			IP = addr[0]
			add := net.ParseIP(IP)
			if add == nil {
				return "", "", "", "", false
			}

			if Type == "http" {
				Port = "80"
			} else if Type == "https" {
				Port = "443"
			}
		}
	case 2:
		{
			IP = addr[0]
			add := net.ParseIP(IP)
			if add == nil {
				return "", "", "", "", false
			}

			Port = addr[1]
			_, err = strconv.Atoi(Port)
			if err != nil {
				return "", "", "", "", false
			}
		}
	default:
		return "", "", "", "", false
	}

	Fmt = fmt.Sprintf("%v://%v:%v", Type, IP, Port)
	return Type, IP, Port, Fmt, true
}

// CatchError .
func CatchError() {
	if r := recover(); r != nil {
		s := fmt.Sprintf("[ERROR] => [%v]", r)
		log.Printf("+%s+", strings.Repeat("-", len(s)))
		log.Printf(s)
		log.Printf("+%s+", strings.Repeat("-", len(s)))
		log.Print(string(debug.Stack()))
		log.Printf("+%s+", strings.Repeat("-", len(s)))
	}
}
