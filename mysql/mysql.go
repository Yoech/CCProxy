package mysql

import (
	"database/sql"
	"errors"
	"fmt"
	// xxx
	_ "github.com/go-sql-driver/mysql"
	"log"
	"strconv"
	"time"
)

// MySQLError .
var MySQLError = []string{
	"pool is nil",
}

// TagMySQLPoolInfo .
type TagMySQLPoolInfo struct {
	Addr     string `yaml:"addr"`
	Port     int    `yaml:"port"`
	DB       string `yaml:"db"`
	User     string `yaml:"user"`
	Pass     string `yaml:"pass"`
	Charset  string `yaml:"charset"`
	MaxConn  int    `yaml:"maxconn"`
	IdleConn int    `yaml:"idleconn"`

	DriverName     string
	DataSourceName string
	SQLDB          *sql.DB
}

// NewPool .
func NewPool(addr string, port int, database string, user string, pass string, charset string, maxOpenConns int, maxIdleConns int) *TagMySQLPoolInfo {
	dataSourceName := fmt.Sprintf("%v:%v@tcp(%v:%v)/%s?charset=%v&autocommit=true&multiStatements=true", user, pass, addr, port, database, charset)
	db := &TagMySQLPoolInfo{
		Addr:     addr,
		Port:     port,
		DB:       database,
		User:     user,
		Pass:     pass,
		Charset:  charset,
		MaxConn:  maxOpenConns,
		IdleConn: maxIdleConns,

		DriverName:     "mysql",
		DataSourceName: dataSourceName,
	}
	if err := db.Open(); err != nil {
		log.Printf("NewPool.err[%v]", err.Error())
		return nil
	}
	return db
}

// Open .
func (p *TagMySQLPoolInfo) Open() error {
	var err error
	p.SQLDB, err = sql.Open(p.DriverName, p.DataSourceName)
	if err != nil {
		return err
	}
	if err = p.SQLDB.Ping(); err != nil {
		return err
	}
	p.SQLDB.SetMaxOpenConns(p.MaxConn)
	p.SQLDB.SetMaxIdleConns(p.IdleConn)
	// 防止断线导致driver: bad connection
	p.SQLDB.SetConnMaxLifetime(30 * time.Second)
	return nil
}

// Close .
func (p *TagMySQLPoolInfo) Close() error {
	if p == nil || p.SQLDB == nil {
		return errors.New("MySQLPool is nil")
	}
	return p.SQLDB.Close()
}

// Get .
func (p *TagMySQLPoolInfo) Get(queryStr string, args ...interface{}) (map[string]interface{}, error) {
	if p == nil || p.SQLDB == nil {
		return nil, errors.New("MySQLPool is nil")
	}

	results, err := p.Query(queryStr, args...)
	if err != nil {
		return nil, err
	}
	if len(results) <= 0 {
		return nil, sql.ErrNoRows
	}
	if len(results) > 1 {
		return nil, errors.New("sql: more than one rows")
	}
	return results[0], nil
}

// Query .
func (p *TagMySQLPoolInfo) Query(queryStr string, args ...interface{}) (rowsMap []map[string]interface{}, err error) {
	var rows *sql.Rows
	var cols []*sql.ColumnType
	if p == nil || p.SQLDB == nil {
		return nil, errors.New("MySQLPool is nil")
	}

	rows, err = p.SQLDB.Query(queryStr, args...)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer rows.Close()
	cols, err = rows.ColumnTypes()
	scanArgs := make([]interface{}, len(cols))
	values := make([]sql.RawBytes, len(cols))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	rowsMap = make([]map[string]interface{}, 0, 10)
	for rows.Next() {
		rows.Scan(scanArgs...)
		rowMap := make(map[string]interface{})
		for i, value := range values {
			rowMap[cols[i].Name()] = bytes2RealType(value, cols[i])
		}
		rowsMap = append(rowsMap, rowMap)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return rowsMap, nil
}

// Exec .
func (p *TagMySQLPoolInfo) Exec(sqlStr string, args ...interface{}) (sql.Result, error) {
	if p == nil || p.SQLDB == nil {
		return nil, errors.New("MySQLPool is nil")
	}
	return p.SQLDB.Exec(sqlStr, args...)
}

// Update .
func (p *TagMySQLPoolInfo) Update(updateStr string, args ...interface{}) (int64, error) {
	if p == nil || p.SQLDB == nil {
		return 0, errors.New("MySQLPool is nil")
	}
	result, err := p.Exec(updateStr, args...)
	if err != nil {
		return 0, err
	}
	affect, err := result.RowsAffected()
	return affect, err
}

// Insert via pool
func (p *TagMySQLPoolInfo) Insert(insertStr string, args ...interface{}) (int64, error) {
	if p == nil || p.SQLDB == nil {
		return 0, errors.New("MySQLPool is nil")
	}
	result, err := p.Exec(insertStr, args...)
	if err != nil {
		return 0, err
	}
	lastID, err := result.LastInsertId()
	return lastID, err

}

// Delete .
func (p *TagMySQLPoolInfo) Delete(deleteStr string, args ...interface{}) (int64, error) {
	if p == nil || p.SQLDB == nil {
		return 0, errors.New("MySQLPool is nil")
	}
	result, err := p.Exec(deleteStr, args...)
	if err != nil {
		return 0, err
	}
	affect, err := result.RowsAffected()
	return affect, err
}

// Transaction .
type Transaction struct {
	SQLTX *sql.Tx
}

// Begin transaction
func (p *TagMySQLPoolInfo) Begin() (*Transaction, error) {
	if p == nil || p.SQLDB == nil {
		return nil, errors.New("MySQLPool is nil")
	}
	var oneSQLConnTransaction = &Transaction{}
	var err error
	if pingErr := p.SQLDB.Ping(); pingErr == nil {
		oneSQLConnTransaction.SQLTX, err = p.SQLDB.Begin()
	}
	return oneSQLConnTransaction, err
}

// Rollback .
func (t *Transaction) Rollback() error {
	if t == nil || t.SQLTX == nil {
		return errors.New("MySQLTransaction is nil")
	}
	return t.SQLTX.Rollback()
}

// Commit .
func (t *Transaction) Commit() error {
	if t == nil || t.SQLTX == nil {
		return errors.New("MySQLTransaction is nil")
	}
	return t.SQLTX.Commit()
}

// Get .
func (t *Transaction) Get(queryStr string, args ...interface{}) (map[string]interface{}, error) {
	if t == nil || t.SQLTX == nil {
		return nil, errors.New("MySQLTransaction is nil")
	}
	results, err := t.Query(queryStr, args...)
	if err != nil {
		return nil, err
	}
	if len(results) <= 0 {
		return nil, sql.ErrNoRows
	}
	if len(results) > 1 {
		return nil, errors.New("sql: more than one rows")
	}
	return results[0], nil
}

// Query .
func (t *Transaction) Query(queryStr string, args ...interface{}) (rowsMap []map[string]interface{}, err error) {
	var rows *sql.Rows
	var cols []*sql.ColumnType
	if t == nil || t.SQLTX == nil {
		return nil, errors.New("MySQLTransaction is nil")
	}
	rows, err = t.SQLTX.Query(queryStr, args...)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer rows.Close()
	cols, err = rows.ColumnTypes()
	scanArgs := make([]interface{}, len(cols))
	values := make([]sql.RawBytes, len(cols))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	rowsMap = make([]map[string]interface{}, 0, 10)
	for rows.Next() {
		rows.Scan(scanArgs...)
		rowMap := make(map[string]interface{})
		for i, value := range values {
			rowMap[cols[i].Name()] = bytes2RealType(value, cols[i])
		}
		rowsMap = append(rowsMap, rowMap)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return rowsMap, nil
}

// Exec .
func (t *Transaction) Exec(sqlStr string, args ...interface{}) (sql.Result, error) {
	if t == nil || t.SQLTX == nil {
		return nil, errors.New("MySQLTransaction is nil")
	}
	return t.SQLTX.Exec(sqlStr, args...)
}

// Update .
func (t *Transaction) Update(updateStr string, args ...interface{}) (int64, error) {
	if t == nil || t.SQLTX == nil {
		return 0, errors.New("MySQLTransaction is nil")
	}
	result, err := t.Exec(updateStr, args...)
	if err != nil {
		return 0, err
	}
	affect, err := result.RowsAffected()
	return affect, err
}

// Insert .
func (t *Transaction) Insert(insertStr string, args ...interface{}) (int64, error) {
	if t == nil || t.SQLTX == nil {
		return 0, errors.New("MySQLTransaction is nil")
	}
	result, err := t.Exec(insertStr, args...)
	if err != nil {
		return 0, err
	}
	lastID, err := result.LastInsertId()
	return lastID, err

}

// Delete .
func (t *Transaction) Delete(deleteStr string, args ...interface{}) (int64, error) {
	if t == nil || t.SQLTX == nil {
		return 0, errors.New("MySQLTransaction is nil")
	}
	result, err := t.Exec(deleteStr, args...)
	if err != nil {
		return 0, err
	}
	affect, err := result.RowsAffected()
	return affect, err
}

func bytes2RealType(src []byte, column *sql.ColumnType) interface{} {
	srcStr := string(src)
	var result interface{}
	switch column.DatabaseTypeName() {
	case "BIT", "TINYINT", "SMALLINT", "INT":
		result, _ = strconv.ParseInt(srcStr, 10, 64)
	case "BIGINT":
		result, _ = strconv.ParseUint(srcStr, 10, 64)
	case "CHAR", "VARCHAR",
		"TINY TEXT", "TEXT", "MEDIUM TEXT", "LONG TEXT",
		"TINY BLOB", "MEDIUM BLOB", "BLOB", "LONG BLOB",
		"JSON", "ENUM", "SET",
		"YEAR", "DATE", "TIME", "TIMESTAMP", "DATETIME":
		result = srcStr
	case "FLOAT", "DOUBLE", "DECIMAL":
		result, _ = strconv.ParseFloat(srcStr, 64)
	default:
		result = nil
	}
	return result
}
